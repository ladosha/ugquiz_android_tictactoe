package ge.msda.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;

    private TextView textViewScore;
    private Button buttonReset;

    private int scorePlayerOne;
    private int scorePlayerTwo;

    List<Button> allButtons;

    private int activePlayer = 1;

    private List<Integer> firstPlayer = new ArrayList<>();
    private List<Integer> secondPlayer = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);

        textViewScore = findViewById(R.id.textViewScore);
        buttonReset = findViewById(R.id.buttonReset);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);

        allButtons  = new ArrayList<>();

        allButtons.add(button1);
        allButtons.add(button2);
        allButtons.add(button3);
        allButtons.add(button4);
        allButtons.add(button5);
        allButtons.add(button6);
        allButtons.add(button7);
        allButtons.add(button8);
        allButtons.add(button9);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Button btn : allButtons) {
                    btn.setEnabled(true);
                    btn.setBackgroundColor(getResources().getColor(R.color.purple_200));
                    btn.setText("");
                }
                firstPlayer.clear();
                secondPlayer.clear();
            }
        });

    }

    @Override
    public void onClick(View clickedView) {

        Button clickedButton = (Button) clickedView;

        int buttonNumber = 0;

        switch (clickedButton.getId()) {
            case R.id.button1:
                buttonNumber = 1;
                break;
            case R.id.button2:
                buttonNumber = 2;
                break;
            case R.id.button3:
                buttonNumber = 3;
                break;
            case R.id.button4:
                buttonNumber = 4;
                break;
            case R.id.button5:
                buttonNumber = 5;
                break;
            case R.id.button6:
                buttonNumber = 6;
                break;
            case R.id.button7:
                buttonNumber = 7;
                break;
            case R.id.button8:
                buttonNumber = 8;
                break;
            case R.id.button9:
                buttonNumber = 9;
                break;
        }

        if (buttonNumber != 0) {
            playGame(buttonNumber, clickedButton);
        }

    }

    private void playGame(int buttonNumber, Button clickedButton) {
        clickedButton.setEnabled(false);
        if (activePlayer == 1) {
            clickedButton.setText("X");
            clickedButton.setBackgroundColor(Color.RED);
            activePlayer = 2;
            firstPlayer.add(buttonNumber);
            check(firstPlayer, 1);
        } else {
            clickedButton.setText("0");
            clickedButton.setBackgroundColor(Color.YELLOW);
            activePlayer = 1;
            secondPlayer.add(buttonNumber);
            check(secondPlayer, 2);
        }
    }

    private void check(List<Integer> args, int player) {


        if (
                        isContainsSet(args, 1, 2, 3) ||
                        isContainsSet(args, 4, 5, 6) ||
                        isContainsSet(args, 7, 8, 9) ||
                        isContainsSet(args, 1, 4, 7) ||
                        isContainsSet(args, 2, 5, 8) ||
                        isContainsSet(args, 3, 6, 9) ||
                        isContainsSet(args, 1, 5, 9) ||
                        isContainsSet(args, 3, 5, 7)

        ) {

            Toast.makeText(this, "Player " + player + " won!", Toast.LENGTH_SHORT).show();
            for (Button btn: allButtons) {
                btn.setEnabled(false);
            }

            if(player == 1) {
                scorePlayerOne++;
            }
            else {
                scorePlayerTwo++;
            }

            textViewScore.setText(String.format("SCORE: %d - %d", scorePlayerOne, scorePlayerTwo));
        }

    }

    private boolean isContainsSet(List<Integer> arr, int... params) {
        for (int param : params) {
            if (!arr.contains(param)) {
                return false;
            }
        }
        return true;
    }

}